package ua.sergeypererva

import scala.util.matching.Regex.Match
import scala.math.abs

/*
 * Here’s an exercise in three parts to do with real world data. Try hard not to read 
 * ahead—do each part in turn.
 * =============================
 * Part One: Weather Data
 * In weather.dat you’ll find daily weather data for Morristown, NJ for June 2002. 
 * Download this text file, then write a program to output the day number (column one) 
 * with the smallest temperature spread (the maximum temperature is the second column, 
 * the minimum the third column).
 * =============================
 * Part Two: Soccer League Table
 * The file football.dat contains the results from the English Premier League for 2001/2. 
 * The columns labeled ‘F’ and ‘A’ contain the total number of goals scored for and against 
 * each team in that season (so Arsenal scored 79 goals against opponents, and had 36 goals 
 * scored against them). Write a program to print the name of the team with the smallest 
 * difference in ‘for’ and ‘against’ goals.
 * ============================
 * Part Three: DRY Fusion
 * Take the two programs written previously and factor out as much common code as possible, 
 * leaving you with two smaller programs and some kind of shared functionality.
 * 
 * URL: http://codekata.pragprog.com/2007/01/kata_four_data_.html
 */
object Kata4 {
  def weatherDataPart = {

    def getFileContents: String = {
      val source = scala.io.Source.fromFile("./data/weather.dat", "utf-8")
      val res = source.getLines.mkString("\n")
      source.close()
      return res
    }

    def extractNeededData(fileContents: String): Map[Int, Double] = {
      val pattern = """\s*(\d{1,2})\s+(\d{1,2})\*?\s+(\d{1,2})\*?\s+.+""".r
      pattern.findAllMatchIn(fileContents).map(
        (m: Match) => (m.subgroups(0).toInt, abs(m.subgroups(1).toDouble - m.subgroups(2).toDouble))).toMap
    }
    extractNeededData(getFileContents).toSeq.sortBy(_._2).head._1
  }

  def footballPart = {
    def getFileContents: String = {
      val source = scala.io.Source.fromFile("./data/football.dat", "utf-8")
      val res = source.getLines.mkString("\n")
      source.close()
      return res
    }

    def extractNeededData(fileContents: String): Map[String, Double] = {
      val pattern = """\s+\d+\.\s+(\w+).+(\d{1,})\s+-\s+(\d{1,}).+""".r
      pattern.findAllMatchIn(fileContents).map(
        (m: Match) => (m.subgroups(0), abs(m.subgroups(1).toDouble - m.subgroups(2).toDouble))).toMap
    }
    extractNeededData(getFileContents).toSeq.sortBy(_._2).head._1
  }
}
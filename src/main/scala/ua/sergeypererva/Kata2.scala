package ua.sergeypererva

/*
 * Write a binary chop method that takes an integer search target and a sorted array of integers. 
 * It should return the integer index of the target in the array, or -1 if the target is not in the array. 
 * The signature will logically be:
 *  
 * chop(int, array_of_int)  -> int
 *    
 * You can assume that the array has less than 100,000 elements. 
 * For the purposes of this Kata, time and memory performance are not issues 
 * assuming the chop terminates before you get bored and kill it, and that you have enough RAM to run it).       
 * 
 * URL: http://codekata.pragprog.com/2007/01/kata_two_karate.html                      
 */
object Kata2 {

  def chop1(el: Int, items: Array[Int]): Int = {
    var res = -1
    var i = 0
    for (item <- items) {
      if (el == item) res = i
      i += 1
    }
    res
  }

  def chop2(el: Int, items: Array[Int]): Int = items match {
    case Array() => -1
    case _ => items.indexOf(el)
  }

  def chop3(el: Int, items: Array[Int]): Int = items match {
    case Array() => -1
    case Array(_) => {
      if (items(0) == el) 0
      else -1
    }
    case _ => {
      val half = items.length / 2
      if (items(half) == el) {
        half
      } else if (items(half) > el) {
        Kata2.chop3(el, items.take(half))
      } else {
        val res = Kata2.chop3(el, items.slice(half, items.length))
        if (res == -1) res else res + half
      }
    }
  }
}
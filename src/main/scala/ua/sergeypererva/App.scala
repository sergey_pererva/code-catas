package ua.sergeypererva

import scala.util.matching.Regex
import scala.util.matching.Regex.Match
import scala.math.abs

/**
 * @author ${user.name}
 */
object App {

  def foo(x: Array[String]) = x.foldLeft("")((a, b) => a + b)

  def plusOne(x: Integer): Integer = x + 1

  def main(args: Array[String]) {
    println(Kata4.weatherDataPart)
    println(Kata4.footballPart)
  }
}

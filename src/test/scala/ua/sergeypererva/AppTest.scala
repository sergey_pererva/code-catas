package ua.sergeypererva

import App._

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
@RunWith(classOf[JUnitRunner])
class AppTest extends FunSuite with ShouldMatchers {
 
  test("A plusOne function should return an input param plus one") {
    plusOne(1) should equal(2)
  }
  
  test("A foo function should return something strange :)") {
    val a = Array("1", "2", "3")
    foo(a) should equal("123")
  }
}


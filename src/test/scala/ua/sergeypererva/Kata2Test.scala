package ua.sergeypererva

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import Kata2._
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Kata2Test extends FunSuite with ShouldMatchers {

  test("A chop1 function should return -1 if item was not found") {
    chop1(1, Array(2, 3, 4)) should equal(-1)
    chop1(3, Array()) should equal(-1)
    chop1(3, Array(1)) should equal(-1)
    val toTest = Array(1, 3, 5)
    for (v <- 0 to 1000 by 2) chop1(v, toTest) should equal(-1)
  }

  test("A chop1 function should return an index of item if it was found") {
    chop1(2, Array(2, 3, 4)) should equal(0)
    val toTest = 0 to 1000 by 4
    for (v <- 0 to 1000 by 4) chop1(v, toTest.toArray) should equal(v / 4)
  }

  test("A chop2 function should return -1 if item was not found") {
    chop2(1, Array(2, 3, 4)) should equal(-1)
    chop2(3, Array()) should equal(-1)
    chop2(3, Array(1)) should equal(-1)
    val toTest = Array(1, 3, 5)
    for (v <- 0 to 1000 by 2) chop2(v, toTest) should equal(-1)
  }

  test("A chop2 function should return an index of item if it was found") {
    chop2(2, Array(2, 3, 4)) should equal(0)
    val toTest = 0 to 1000 by 4
    for (v <- 0 to 1000 by 4) chop2(v, toTest.toArray) should equal(v / 4)
  }

  test("A chop3 function should return -1 if item was not found") {
    chop3(1, Array(2, 3, 4)) should equal(-1)
    chop3(3, Array()) should equal(-1)
    chop3(3, Array(1)) should equal(-1)
    val toTest = Array(1, 3, 5)
    for (v <- 0 to 1000 by 2) chop3(v, toTest) should equal(-1)
  }

  test("A chop3 function should return an index of item if it was found") {
    chop3(2, Array(2, 3, 4)) should equal(0)
    val toTest = 0 to 1000 by 4
    for (v <- 0 to 1000 by 4) chop3(v, toTest.toArray) should equal(v / 4)
  }
}